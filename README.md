# LAME-based MP3 encoder #

## Build instructions ##

### Linux/UNIX ###

    mkdir build
    cd build
    cmake ..
    make
    make test

### Windows - `MSYS2` ###

    mkdir build
    cd build
    cmake -G 'MSYS Makefiles' ..
    make
    make test

### Windows - Visual Studio 2019 ###

From the Visual Studio 2019 developer's `PowerShell`:

    mkdir build
    cd build
    cmake -G 'NMake Makefiles' ..
    nmake
    nmake test

## `wav2mp3` usage ##

Convert all wave files to mp3 in a folder:

    cd <folder>
    wav2mp3
