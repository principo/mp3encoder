#include <iostream>
#include <vector>
#include <cstdlib>
#include "gtest/gtest.h"

#include "mp3encoder.h"
#include "sampletypes.h"

using namespace mp3encoder;

TEST(LameEncoderTest, Initialization)
{
    LameEncoder encoder;

    auto version = encoder.version();
    EXPECT_EQ(version.substr(0, 1), "3");
}

TEST(LameEncoderTest, InitializationErrors)
{
    size_t errors = 0;

    try {
        LameEncoder encoder{0};
    } catch(LameEncoder::Error &) {
        errors++;
    }
    EXPECT_EQ(errors, 1);

    try {
        LameEncoder encoder{1, 0};
    } catch(LameEncoder::Error &) {
        errors++;
    }
    EXPECT_EQ(errors, 2);
}

TEST(LameEncoderTest, Encode)
{
    LameEncoder encoder;
    auto sz = encoder.getMaximumChunkSize();
    std::vector<int> pcmL(sz), pcmR(sz);
    for (size_t i = 0; i < sz; ++i) {
        pcmL[i] = rand();
        pcmR[i] = rand();
    }

    auto out = encoder.encodeChunk(pcmL, pcmR);
    EXPECT_GT(out.size(), 0);
    EXPECT_LT(out.size(), 1.25 * sz + 7200);
}

struct WaveFileInfo
{
    const char *name;
    WaveFile::Format format;
    size_t channels;
    size_t bits;
    size_t sampleRate;
};

static std::initializer_list<WaveFileInfo> waveFiles = {
    {"data/M1F1-float32-AFsp.wav", WaveFile::Format::IEEE_FLOAT, 2, 32, 8000},
    {"data/M1F1-float64-AFsp.wav", WaveFile::Format::IEEE_FLOAT, 2, 64, 8000},
    {"data/M1F1-uint8-AFsp.wav",   WaveFile::Format::PCM,        2, 8,  8000},
    {"data/M1F1-int12-AFsp.wav",   WaveFile::Format::PCM,        2, 12, 8000},
    {"data/M1F1-int16-AFsp.wav",   WaveFile::Format::PCM,        2, 16, 8000},
    {"data/M1F1-int24-AFsp.wav",   WaveFile::Format::PCM,        2, 24, 8000},
    {"data/M1F1-int32-AFsp.wav",   WaveFile::Format::PCM,        2, 32, 8000},
};

TEST(WaveFile, Open)
{
    for (auto &file : waveFiles) {
        WaveFile wave{file.name};
    }
}

TEST(WaveFile, Read)
{
    using namespace sampletypes;
    for (auto &file : waveFiles) {
        WaveFile wave{file.name};
        EXPECT_EQ(wave.format(), file.format);
        EXPECT_EQ(wave.channels(), file.channels);
        EXPECT_EQ(wave.bits(), file.bits);
        EXPECT_EQ(wave.sampleRate(), file.sampleRate);
        ASSERT_GT(wave.samples(), 0);

        // read whole wave file
        auto len = wave.samples() * wave.channels();
        if (wave.format() == WaveFile::Format::PCM) {
            if (wave.bits() == 8) {
                std::vector<uint8_t> buf(len);
                EXPECT_EQ(wave.read(buf), true);
                EXPECT_EQ(buf.size(), len);
                EXPECT_EQ(wave.read(buf), false);
            } else if (wave.bits() == 12) {
                std::vector<int16_t> buf(len);
                EXPECT_EQ(wave.read(buf), true);
                EXPECT_EQ(buf.size(), len);
                EXPECT_EQ(wave.read(buf), false);
            } else if (wave.bits() == 16) {
                std::vector<int16_t> buf(len * 2);
                EXPECT_EQ(wave.read(buf), true);
                EXPECT_EQ(buf.size(), len);
                EXPECT_EQ(wave.read(buf), false);
            } else if (wave.bits() == 24) {
                std::vector<Int24> buf(len);
                EXPECT_EQ(wave.read(buf), true);
                EXPECT_EQ(buf.size(), len);
                EXPECT_EQ(wave.read(buf), false);
            } else if (wave.bits() == 32) {
                std::vector<int32_t> buf(len);
                EXPECT_EQ(wave.read(buf), true);
                EXPECT_EQ(buf.size(), len);
                EXPECT_EQ(wave.read(buf), false);
            } else {
                FAIL();
            }
        } else if (wave.format() == WaveFile::Format::IEEE_FLOAT) {
            if (wave.bits() == 32) {
                std::vector<float> buf(len);
                EXPECT_EQ(wave.read(buf), true);
                EXPECT_EQ(buf.size(), len);
                EXPECT_EQ(wave.read(buf), false);
            } else if (wave.bits() == 64) {
                std::vector<double> buf(len);
                EXPECT_EQ(wave.read(buf), true);
                EXPECT_EQ(buf.size(), len);
                EXPECT_EQ(wave.read(buf), false);
            } else {
                FAIL();
            }
        } else {
            FAIL();
        }
    }
}

TEST(SampleTypes, ToInt)
{
    using namespace sampletypes;

    EXPECT_EQ(UInt8{UInt8::MAX}.toInt(), std::numeric_limits<int>::max());
    EXPECT_EQ(UInt8{UInt8::MIN}.toInt(), std::numeric_limits<int>::min());

    EXPECT_EQ(Int12{0}.toInt(), 0);
    EXPECT_EQ(Int12{Int12::MAX}.toInt(), std::numeric_limits<int>::max());
    EXPECT_EQ(Int12{Int12::MIN}.toInt(), std::numeric_limits<int>::min());

    EXPECT_EQ(Int16{0}.toInt(), 0);
    EXPECT_EQ(Int16{Int16::MAX}.toInt(), std::numeric_limits<int>::max());
    EXPECT_EQ(Int16{Int16::MIN}.toInt(), std::numeric_limits<int>::min());

    EXPECT_EQ(Int24{0}.toInt(), 0);
    EXPECT_EQ(Int24{Int24::MAX}.toInt(), std::numeric_limits<int>::max());
    EXPECT_EQ(Int24{Int24::MIN}.toInt(), std::numeric_limits<int>::min());

    EXPECT_EQ(Int32{0}.toInt(), 0);
    EXPECT_EQ(Int32{Int32::MAX}.toInt(), std::numeric_limits<int>::max());
    EXPECT_EQ(Int32{Int32::MIN}.toInt(), std::numeric_limits<int>::min());

    EXPECT_EQ(Float32{0}.toInt(), 0);
    EXPECT_EQ(Float32{Float32::MAX}.toInt(), std::numeric_limits<int>::max());
    EXPECT_EQ(Float32{Float32::MIN}.toInt(), std::numeric_limits<int>::min());

    EXPECT_EQ(Float64{0}.toInt(), 0);
    EXPECT_EQ(Float64{Float64::MAX}.toInt(), std::numeric_limits<int>::max());
    EXPECT_EQ(Float64{Float64::MIN}.toInt(), std::numeric_limits<int>::min());
}

TEST(WaveData, Iterator)
{
    for (auto &file : waveFiles) {
        WaveFile wave{file.name};
        WaveData data{wave, 1024};
        size_t nSamples = 0;
        for (auto &chunk : data) {
            nSamples += chunk.left.size();
        }
        EXPECT_EQ(wave.samples(), nSamples);
    }
}

int main(int argc, char **argv) {
    testing::InitGoogleTest(&argc, argv);
    return RUN_ALL_TESTS();
}
