#ifndef LZYHL5WXZ9AVN5L19Q6OPNK66P6C2NHU6U83WUZT
#define LZYHL5WXZ9AVN5L19Q6OPNK66P6C2NHU6U83WUZT

#include <memory>
#include <fstream>
#include <string>
#include <type_traits>

namespace mp3encoder {

class IFFChunk
{
public:
    struct Error : public std::runtime_error { Error(const std::string &); };

    enum class Whence { BEGIN, END, CURRENT };

    IFFChunk();
    IFFChunk(std::shared_ptr<std::ifstream> file);

    // move semantics
    IFFChunk(IFFChunk &&other);
    IFFChunk &operator=(IFFChunk &&other);

    ~IFFChunk();

    void open(std::shared_ptr<std::ifstream> file);
    void close();

    const std::string &id();
    size_t size();

    void seek(int pos, Whence whence = Whence::BEGIN);

    template <typename T>
    std::enable_if_t<std::is_integral<T>::value || std::is_pod<T>::value, bool>
    read(T &val)
    {
        auto n = read(reinterpret_cast<char *>(&val), sizeof(T));
        return (n == sizeof(T));
    }

    template <typename T>
    bool read(T &buf, typename T::iterator * = nullptr)
    {
        size_t sz = buf.size() * sizeof(buf[0]);
        auto n = read(reinterpret_cast<char *>(&buf[0]), sz);
        buf.resize(n / sizeof(buf[0]));
        return (n > 0);
    }

protected:
    size_t read(char *buf, size_t size);

private:
    struct Impl;
    std::unique_ptr<Impl> impl;
};

} // namespace mp3encoder

#endif // LZYHL5WXZ9AVN5L19Q6OPNK66P6C2NHU6U83WUZT
