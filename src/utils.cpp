#include "utils.h"

using namespace utils;

std::mutex AsyncLog::mutex_;

#ifdef _WIN32

#include <windows.h>

std::vector<std::string> utils::readDir(const std::string &path)
{
    std::vector<std::string> files;

    std::string pattern{path};
    pattern.append("\\*");
    WIN32_FIND_DATA fd;
    HANDLE fh;
    if ((fh = FindFirstFile(pattern.c_str(), &fd)) != INVALID_HANDLE_VALUE) {
        do {
            files.push_back(fd.cFileName);
        } while (FindNextFile(fh, &fd));
        FindClose(fh);
    }

    return files;
}

#include <direct.h>

bool utils::changeDir(const std::string &path)
{
    return (::_chdir(path.c_str()) == 0);
}

#else

#include <unistd.h>
#include <dirent.h>

std::vector<std::string> utils::readDir(const std::string &path)
{
    std::vector<std::string> files;


    auto dir = opendir(path.c_str());
    while (auto dp = readdir(dir)) {
        files.push_back(dp->d_name);
    }
    closedir(dir);

    return files;
}

bool utils::changeDir(const std::string &path)
{
    return (::chdir(path.c_str()) == 0);
}

#endif // _WIN32
