#include <array>
#include <vector>
#include <fstream>
#include <sstream>
#include <type_traits>

#include "mp3encoder.h"

using namespace mp3encoder;

WaveFile::Error::Error(const std::string &msg)
    : std::runtime_error{"WaveFile: " + msg}
{ }

struct WaveFile::Impl
{
    #pragma pack(1)
    struct Fmt {
        constexpr static const char *id = "fmt ";
        uint16_t formatTag;
        uint16_t channels;
        uint32_t samplesPerSec;
        uint32_t avgBytesPerSec;
        uint16_t blockAlign;
        uint16_t bitsPerSample;
    };

    struct Fact {
        constexpr static const char *id = "fact";
    };

    struct Data {
        constexpr static const char *id = "data";
    };
    #pragma pack()

    Impl(const std::string &fileName)
    {
        if (fileName.empty()) return;

        auto file = std::make_shared<std::ifstream>(fileName, std::ios::binary);
        if (!file->is_open()) {
            std::stringstream msg;
            msg << "Can't open " << fileName;
            throw Error{msg.str()};
        }

        IFFChunk riff{file};
        std::string waveId(4, ' ');
        if (!riff.read(waveId)) {
            throw Error{"WAVE file is too short"};
        }

        if (riff.id() != "RIFF" || waveId != "WAVE") {
            std::stringstream msg;
            msg << fileName << " is not a WAVE file";
            throw Error{msg.str()};
        }

        while (!file->eof()) {
            IFFChunk chunk{file};

            if (chunk.id() == Data::id) {
                data_ = std::move(chunk);
                break;
            }

            readInfoChunk(chunk);
            chunk.close();
        }
    }

    void readInfoChunk(IFFChunk &chunk)
    {
        if (chunk.size() > 40) {
            std::stringstream msg;
            msg << "Bad WAVE chunk size [" << chunk.size() << "]";
            throw Error{msg.str()};
        }

        if (chunk.id() == Fmt::id) {
            if (!chunk.read(format_)) {
                std::stringstream msg;
                msg << "Fmt chunk is too small [" << chunk.size()    << " < "
                                                  << sizeof(format_) << "]";
                throw Error{msg.str()};
            }
        } else if (chunk.id() == Fact::id) {
        } else {
            std::stringstream msg;
            msg << "Unknown chunk ID [" << chunk.id() << "]";
            throw Error{msg.str()};
        }
    }

    auto format()     { return format_.formatTag; }
    auto channels()   { return format_.channels; }
    auto bits()       { return format_.bitsPerSample; }
    auto sampleRate() { return format_.samplesPerSec; }

    auto samples()
    {
        size_t bytes = format_.bitsPerSample / 8;
        if (format_.bitsPerSample % 8) bytes++;
        return data_.size() / (format_.channels * bytes);
    }

    IFFChunk &data() { return data_; }

private:
    Fmt format_;
    IFFChunk data_;
};

WaveFile::WaveFile(const std::string &fileName)
    : impl{new Impl{fileName}}
{ }

WaveFile::Format WaveFile::format()
{
    return static_cast<Format>(impl->format());
}

size_t WaveFile::channels()
{
    return static_cast<size_t>(impl->channels());
}

size_t WaveFile::bits()
{
    return static_cast<size_t>(impl->bits());
}

size_t WaveFile::sampleRate()
{
    return static_cast<size_t>(impl->sampleRate());
}

size_t WaveFile::samples()
{
    return static_cast<size_t>(impl->samples());
}

void WaveFile::rewind()
{
    impl->data().seek(0);
}

IFFChunk &WaveFile::dataChunk()
{
    return impl->data();
}

WaveFile::~WaveFile() = default;
