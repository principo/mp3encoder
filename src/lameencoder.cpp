#include <vector>
#include <sstream>
#include "lame.h"
#include "mp3encoder.h"

using namespace mp3encoder;

LameEncoder::Error::Error(const std::string &msg)
    : std::runtime_error{"LameEncoder: " + msg}
{ }

struct LameEncoder::Impl {
    Impl()
        : gfp{lame_init()}
    {
        if (!gfp) throw Error{"Can't initialize the library."};
    }

    ~Impl()
    {
        lame_close(gfp);
    }

    void setInSamplerate(int inSampleRate)
    {
        if (lame_set_in_samplerate(gfp, inSampleRate) < 0) {
            std::stringstream msg;
            msg << "Can't set the input sample rate [" << inSampleRate << "]";
            throw Error{msg.str()};
        }
    }

    void setNumChannels(int nChannels)
    {
        if (lame_set_num_channels(gfp, nChannels) < 0) {
            std::stringstream msg;
            msg << "Can't set the number of channels [" << nChannels << "]";
            throw Error{msg.str()};
        }

    }

    void setQuality(int quality)
    {
        if (lame_set_quality(gfp, quality) < 0) {
            std::stringstream msg;
            msg << "Can't set the internal algorithm [" << quality << "]";
            throw Error{msg.str()};
        }
    }

    void setVBR(bool enabled)
    {
        if (lame_set_VBR(gfp, enabled ? vbr_default : vbr_off) < 0) {
            std::stringstream msg;
            msg << "Can't " << (enabled ? "enable" : "disable") << " VBR mode";
            throw Error{msg.str()};
        }
    }

    void setVBRQuality(int quality)
    {
        if (lame_set_VBR_q(gfp, quality) < 0) {
            std::stringstream msg;
            msg << "Can't set VBR quality [" << quality << "]";
            throw Error{msg.str()};
        }
    }

    void initParams()
    {
        if (lame_init_params(gfp) < 0) {
            throw Error{"Can't initialize LAME parameters"};
        }
    }

    size_t getMaximumNumberOfSamples()
    {
        int ret = lame_get_maximum_number_of_samples(gfp, LAME_MAXMP3BUFFER);
        if (ret < 0) {
            throw Error{"Can't get the maximum number of samples"};
        }
        return static_cast<size_t>(ret);
    }

    std::vector<uint8_t> encodeBuffer(const std::vector<int> &pcmL,
                                      const std::vector<int> &pcmR)
    {
        std::vector<uint8_t> out;
        out.resize(LAME_MAXMP3BUFFER);

        int ret = lame_encode_buffer_int(gfp, pcmL.data(), pcmR.data(),
                                         static_cast<int>(pcmL.size()),
                                         out.data(), LAME_MAXMP3BUFFER);
        if (ret < 0) {
            std::stringstream msg;
            if (ret == -1) {
                msg << "MP3 buffer is not big enough";
            } else {
                msg << "Can't encode the buffer. Error code: " << ret;
            }
            throw Error{msg.str()};
        }

        out.resize(static_cast<size_t>(ret));
        return out;
    }

    std::vector<uint8_t> flush()
    {
        std::vector<uint8_t> out;
        out.reserve(7200);

        int ret = lame_encode_flush(gfp,  out.data(),
                                    static_cast<int>(out.size()));
        if (ret < 0) {
            throw Error{"Flush error"};
        }

        out.resize(static_cast<size_t>(ret));
        return out;
    }

private:
    lame_global_flags *gfp;
};

LameEncoder::LameEncoder(size_t nChannels, size_t inSampleRate,  Quality quality)
    : impl{new Impl}
{
    impl->setNumChannels(static_cast<int>(nChannels));
    impl->setInSamplerate(static_cast<int>(inSampleRate));

    impl->setQuality(static_cast<int>(quality));
    impl->setVBR(true);
    impl->setVBRQuality(static_cast<int>(quality));

    impl->initParams();
}

size_t LameEncoder::getMaximumChunkSize()
{
    return impl->getMaximumNumberOfSamples();
}

std::vector<uint8_t> LameEncoder::encodeChunk(const std::vector<int> &pcmL,
                                              const std::vector<int> &pcmR)
{
    if (pcmL.size() != pcmR.size()) {
        std::stringstream msg;
        msg << "Left and right channels contain different number of samples"
            << " [" << pcmL.size() << " != " << pcmR.size() << "]";
        throw Error{msg.str()};
    }

    if (pcmL.size() > impl->getMaximumNumberOfSamples()) {
        std::stringstream msg;
        msg << "The chunk size [" << pcmL.size() << "] is too big";
        throw Error{msg.str()};
    }

    return impl->encodeBuffer(pcmL, pcmR);
}

std::vector<uint8_t> LameEncoder::flush()
{
    return impl->flush();
}

std::string LameEncoder::version()
{
    return get_lame_version();
}

LameEncoder::~LameEncoder() = default;
