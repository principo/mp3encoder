#ifndef WMZTLCVYSEJDIPWWUQKVWMOIISIFTSYKLXOZVXFM
#define WMZTLCVYSEJDIPWWUQKVWMOIISIFTSYKLXOZVXFM

#include <mutex>
#include <iostream>
#include <vector>
#include <string>

namespace utils {

struct AsyncLog
{
    AsyncLog(std::ostream &outStream = std::cout,
             std::ostream &errStream = std::cerr,
             std::ostream &logStream = std::clog)
        : out{outStream}
        , err{errStream}
        , log{logStream}
        , lock_{mutex_}
    { }

    std::ostream &out;
    std::ostream &err;
    std::ostream &log;

private:
    static std::mutex mutex_;
    std::unique_lock<std::mutex> lock_;
};

#define Log AsyncLog{}

std::vector<std::string> readDir(const std::string &path);
bool changeDir(const std::string &path);

} // namespace utils

#endif // WMZTLCVYSEJDIPWWUQKVWMOIISIFTSYKLXOZVXFM
