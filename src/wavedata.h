#ifndef DDO6A55695QZQ6P58MNHKDS6SB1IUEUBHJRPGQ8Z
#define DDO6A55695QZQ6P58MNHKDS6SB1IUEUBHJRPGQ8Z

#include <memory>

#include "wavefile.h"
#include "wavedataiterator.h"

namespace mp3encoder {

class WaveData {
public:
    using iterator = WaveDataIterator;

    explicit WaveData(WaveFile &file, size_t chunkSize);
    ~WaveData();

    iterator begin();
    iterator end();

private:
    struct Impl;
    std::unique_ptr<Impl> impl;
};

} // namespace mp3encoder

#endif // DDO6A55695QZQ6P58MNHKDS6SB1IUEUBHJRPGQ8Z
