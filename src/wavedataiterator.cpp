#include <sstream>
#include "sampletypes.h"
#include "wavedataiterator.h"

using namespace mp3encoder;

#define INVOKE_SELF(memFn, ...) ((memFn)?(this->*(memFn))(__VA_ARGS__):0)

WaveDataIterator::Error::Error(const std::string &msg)
    : std::runtime_error{"WaveDataIterator: " + msg}
{ }

struct WaveDataIterator::Impl
{
    using ChunkReader = bool (Impl::*)();


    Impl(WaveFile &file, size_t sz)
        : file_{file}
        , chunkSize_{sz}
    {
        if (!chunkSize_) return;

        file_.rewind();
        setChunkReader();
    }

    const Chunk &chunk() const { return chunk_; }

    template<typename T>
    bool readChunk()
    {
        auto nChannels = file_.channels();
        std::vector<T> buf(chunkSize_ * nChannels);
        if (!file_.read(buf)) return false;

        auto nSamples = buf.size() / nChannels;
        chunk_.left.resize(nSamples);
        chunk_.right.resize(nSamples);

        size_t n = 0;
        for (size_t i = 0; i < nSamples; ++i) {
            for (size_t k = 0; k < nChannels; ++k, ++n) {
                if (k == 0) chunk_.left[i] = buf[n].toInt();
                else if (k == 1) chunk_.right[i] = buf[n].toInt();
            }
        }
        return true;
    }

    void getNextChunk() {
        if (!chunkSize_) return;

        if (!INVOKE_SELF(chunkReader_)) {
            chunk_.left.clear();
            chunk_.right.clear();
        }
    }

    void setChunkReader()
    {
        using namespace sampletypes;
        if (file_.format() == WaveFile::Format::PCM) {
            switch (file_.bits()) {
                case 8:  chunkReader_ = &Impl::readChunk<UInt8>; break;
                case 12: chunkReader_ = &Impl::readChunk<Int12>; break;
                case 16: chunkReader_ = &Impl::readChunk<Int16>; break;
                case 24: chunkReader_ = &Impl::readChunk<Int24>; break;
                case 32: chunkReader_ = &Impl::readChunk<Int32>; break;
                default:
                    std::stringstream msg;
                    msg << "Unsupported PCM sample length ["
                        << file_.bits() << "]";
                    throw Error{msg.str()};
            }
        } else if (file_.format() == WaveFile::Format::IEEE_FLOAT) {
            switch (file_.bits()) {
                case 32: chunkReader_ = &Impl::readChunk<Float32>; break;
                case 64: chunkReader_ = &Impl::readChunk<Float64>; break;
                default:
                    std::stringstream msg;
                    msg << "Unsupported IEEE_FLOAT sample length ["
                        << file_.bits() << "]";
                    throw Error{msg.str()};
            }
        } else {
            std::stringstream msg;
            msg << "Unsupported data format ["
                << static_cast<int>(file_.format()) << "]";
            throw Error{msg.str()};
        }
        getNextChunk();
    }

private:
    WaveFile &file_;
    size_t chunkSize_;
    Chunk chunk_;
    ChunkReader chunkReader_ = nullptr;
};

static WaveFile emptyWave{};

WaveDataIterator::WaveDataIterator()
    :impl{new Impl{emptyWave, 0}}
{
}

WaveDataIterator::WaveDataIterator(WaveFile &file, size_t sz)
    : impl{new Impl{file, sz}}
{  }

WaveDataIterator &WaveDataIterator::operator++()
{
    impl->getNextChunk();
    return *this;
}

bool WaveDataIterator::operator!=(WaveDataIterator &other) const
{
    auto &a = impl->chunk();
    auto &b = other.impl->chunk();

    return a.left != b.left || a.right != b.right;
}

const WaveDataIterator::Chunk &WaveDataIterator::operator*() const
{
    return impl->chunk();
}
