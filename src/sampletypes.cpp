#include <cstdint>
#include <array>
#include "sampletypes.h"

using namespace mp3encoder::sampletypes;

Int24::Int24(int32_t value)
{
    for (auto &b : value_) {
        b = static_cast<uint8_t>(value);
        value >>= 8;
    }
}

int Int24::toInt()
{
    constexpr int shift = sizeof(int) - sizeof(value_);
    int v{0};
    for (size_t i = value_.size(); i > 0; --i) {
        v |= value_[i - 1] << (8 * (i - 1 + shift));
    }
    v >>= 8 * shift;
    return Signed<int, 24>{v}.toInt();
}
