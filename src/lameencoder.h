#ifndef PZSPBVTP418IET6W978VTHQ8E1HHF4XI0OJ6NDKO
#define PZSPBVTP418IET6W978VTHQ8E1HHF4XI0OJ6NDKO

#include <memory>
#include <vector>
#include <string>
#include <exception>

namespace mp3encoder {

class LameEncoder {
public:
    struct Error : public std::runtime_error { Error(const std::string &); };

    enum class Quality { BEST = 2, GOOD = 5, OK = 7 };

    LameEncoder(size_t nChannels = 2, size_t inSampleRate = 44100,
                Quality quality = Quality::GOOD);
    ~LameEncoder();

    size_t getMaximumChunkSize();
    std::vector<uint8_t> encodeChunk(const std::vector<int> &pcmLeft,
                                     const std::vector<int> &pcmRight);
    std::vector<uint8_t> flush();

    std::string version();

private:
    struct Impl;
    std::unique_ptr<Impl> impl;
};

} // namespace mp3encoder

#endif // PZSPBVTP418IET6W978VTHQ8E1HHF4XI0OJ6NDKO
