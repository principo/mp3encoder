#ifndef XUPDW9UREZO3EJL2HY8VAW9FAETITA2UUOW9LU3N
#define XUPDW9UREZO3EJL2HY8VAW9FAETITA2UUOW9LU3N

#include <memory>
#include <vector>
#include <stdexcept>

#include "wavefile.h"

namespace mp3encoder {

class WaveDataIterator {
public:
    struct Error : public std::runtime_error { Error(const std::string &); };

    struct Chunk {
        std::vector<int> left;
        std::vector<int> right;
    };

    WaveDataIterator();
    explicit WaveDataIterator(WaveFile &file, size_t sz);

    WaveDataIterator &operator++();
    bool operator!=(WaveDataIterator &other) const;
    const Chunk &operator*() const;

private:
    struct Impl;
    std::shared_ptr<Impl> impl;
};

} // namespace mp3encoder

#endif // XUPDW9UREZO3EJL2HY8VAW9FAETITA2UUOW9LU3N
