#include "wavedata.h"

using namespace mp3encoder;

struct WaveData::Impl {
    Impl(WaveFile &file, size_t sz)
        : file_{file}
        , chunkSize_{sz}
    { }

    iterator begin()
    {
        return iterator{file_, chunkSize_};
    }

    iterator end()
    {
        return iterator{};
    }

private:
    WaveFile &file_;
    size_t chunkSize_;
};

WaveData::WaveData(WaveFile &file, size_t sz)
    : impl{new Impl{file, sz}}
{  }

WaveData::iterator WaveData::begin()
{
    return impl->begin();
}

WaveData::iterator WaveData::end()
{
    return impl->end();
}

WaveData::~WaveData() = default;
