#include <exception>
#include <sstream>
#include <fstream>
#include <array>
#include "mp3encoder.h"

using namespace mp3encoder;

IFFChunk::Error::Error(const std::string &msg)
    : std::runtime_error{"IFFChunk: " + msg}
{ }

struct IFFChunk::Impl
{
    void open(std::shared_ptr<std::ifstream> file)
    {
        if (!file || !file->is_open()) throw Error{"File is not opened"};

        file_ = file;
        file_->exceptions(std::ios::failbit|std::ios::eofbit);

        try {
            std::array<char, 4> id;
            file_->read(id.data(), id.size());
            id_ = std::string(id.begin(), id.end());

            std::array<char, 4> size;
            file_->read(size.data(), size.size());
            size_ = *reinterpret_cast<uint32_t *>(size.data());

            payload_ = file_->tellg();
            file_->seekg(static_cast<int>(size_), std::ios::cur);
            end_ = file_->tellg();
            file_->seekg(payload_);

            auto payloadSize = static_cast<size_t>(end_ - payload_);
            if (payloadSize != size_) {
                std::stringstream msg;
                msg << "Bad chunk size: " << size_ << " != " << payloadSize;
                throw Error{msg.str()};
            }
        } catch (const std::ios_base::failure &ex) {
            std::stringstream msg;
            msg << "Construction error: " << ex.what();
            throw Error{msg.str()};
        }

    }

    const std::string &id()
    {
        return id_;
    }

    size_t size()
    {
        return size_;
    }

    void seek(int pos, Whence whence)
    {
        if (!file_) throw Error{"Chunk is not opened [seek]"};

        try {
            if (whence == Whence::BEGIN) {
                file_->seekg(payload_);
            } else if (whence == Whence::END) {
                file_->seekg(end_);
            }
            file_->seekg(pos, std::ios::cur);
        } catch (const std::ios_base::failure &ex) {
            std::stringstream msg;
            msg << "Seek error: " << ex.what();
            throw Error{msg.str()};
        }
    }

    void close()
    {
        if (!file_) throw Error{"Chunk is not opened [close]"};

        file_->seekg(end_);
    }

    long read(char *buf, long size)
    {
        if (!file_) throw Error{"Chunk is not opened [read]"};

        try {
            auto restSize = end_ - file_->tellg();
            if (!restSize) return 0;
            if (!size || size > restSize) size = restSize;

            file_->read(buf, size);
            return size;
        } catch (const std::ios_base::failure &ex) {
            std::stringstream msg;
            msg << "Read error: " << ex.what();
            throw Error{msg.str()};
        }
    }

private:
    std::shared_ptr<std::ifstream> file_;
    std::string id_;
    size_t size_ = 0;
    std::ifstream::pos_type payload_;
    std::ifstream::pos_type end_;
};

IFFChunk::IFFChunk()
    : impl{new Impl}
{ }

IFFChunk::IFFChunk(std::shared_ptr<std::ifstream> file)
    : IFFChunk{}
{
    impl->open(file);
}

IFFChunk::IFFChunk(IFFChunk &&other)
    : impl{std::move(other.impl)}
{ }

IFFChunk &IFFChunk::operator=(IFFChunk &&other)
{
    impl = std::move(other.impl);
    return *this;
}

void IFFChunk::open(std::shared_ptr<std::ifstream> file)
{
    impl->open(file);
}

IFFChunk::~IFFChunk() = default;

const std::string &IFFChunk::id()
{
    return impl->id();
}

size_t IFFChunk::size()
{
    return impl->size();
}

void IFFChunk::seek(int pos, IFFChunk::Whence whence)
{
    impl->seek(pos, whence);
}

void IFFChunk::close()
{
    impl->close();
}

size_t IFFChunk::read(char *buf, size_t size)
{
    return static_cast<size_t>(impl->read(buf, static_cast<long>(size)));
}
