#include <iostream>
#include <fstream>
#include <sstream>
#include <string>
#include <vector>
#include <thread>

#include "utils.h"
#include "mp3encoder.h"

void encodeWaveFile(const std::string &wavFile, const std::string &mp3File)
{
    try {
        mp3encoder::WaveFile wave{wavFile};
        mp3encoder::LameEncoder encoder{wave.channels(), wave.sampleRate()};
        mp3encoder::WaveData waveData{wave, encoder.getMaximumChunkSize()};

        std::ofstream out{mp3File, std::ios::binary};
        if (!out.is_open()) {
            utils::Log.err << "Can't create " << mp3File << std::endl;
            return;
        }
        out.exceptions(std::ios::failbit | std::ios::eofbit);

        for (auto &chunk : waveData) {
            auto buf = encoder.encodeChunk(chunk.left, chunk.right);
            out.write(reinterpret_cast<char *>(buf.data()),
                      static_cast<long>(buf.size()));
        }

        auto buf = encoder.flush();
        out.write(reinterpret_cast<char *>(buf.data()),
                  static_cast<long>(buf.size()));

        utils::Log.out << wavFile << ": OK" << std::endl;
    } catch (const std::runtime_error &ex) {
        utils::Log.out << wavFile << ": Failed: " << ex.what() << std::endl;
    }
}

int main(int, const char *argv[])
{
    if (auto workDir = argv[1]) {
        if (!utils::changeDir(workDir)) {
            utils::Log.err << "Can't open " << argv[1] << std::endl;
            exit(1);
        }
    }

    auto files = utils::readDir(".");
    std::vector<std::thread> threads;

    for (auto &wavFile : files) {
        auto pos = wavFile.rfind(".wav");
        if (pos == std::string::npos) continue;

        std::string mp3File{wavFile};
        mp3File.replace(pos, 4, ".mp3");

        utils::Log.out << "Converting " << wavFile << " to " << mp3File
                       << "..." << std::endl;
        threads.emplace_back(encodeWaveFile, wavFile, mp3File);
    }

    for (auto &t : threads) t.join();
    return 0;
}
