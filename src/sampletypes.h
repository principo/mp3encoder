#ifndef Q44S7EU5VTZ6QXJLCLJ5242G9TYJU5X2ZDQ1V5MP
#define Q44S7EU5VTZ6QXJLCLJ5242G9TYJU5X2ZDQ1V5MP

#include <cstdint>
#include <cmath>
#include <limits>
#include <array>
#include <type_traits>

namespace mp3encoder {

namespace sampletypes {

constexpr int pow(int val, int exp)
{
    return (exp == 0) ? 1 : val * pow(val, exp - 1);
}

template <typename T, int BITS = sizeof(T) * 8,
          typename = std::enable_if_t<std::is_unsigned<T>::value>>
struct Unsigned
{
    constexpr static auto MAX = pow(2, BITS) - 1;
    constexpr static auto MIN = 0;

    int toInt()
    {
        constexpr auto shift = sizeof(int) * 8 - BITS;
        int v{value_};
        v = v > 0 ? ((v + 1) << shift) - 1 : v << shift;
        return static_cast<int>(v + std::numeric_limits<signed>::min());
    }

    T value_;
};

template <typename T, int BITS = sizeof(T) * 8,
          typename = std::enable_if_t<std::is_signed<T>::value>>
struct Signed
{
    constexpr static auto MAX = pow(2, BITS-1) - 1;
    constexpr static auto MIN = -pow(2, BITS-1);

    int toInt()
    {
        constexpr auto SHIFT = sizeof(int) * 8 - BITS;
        int v{value_};
        return static_cast<int>(v > 0 ? ((v + 1) << SHIFT) - 1 : v << SHIFT);
    }

    T value_;
};

template <>
struct Signed<int> {
    constexpr static auto MAX = std::numeric_limits<int>::max();
    constexpr static auto MIN = std::numeric_limits<int>::min();

    int toInt() { return value_; }
    int value_;
};

using UInt8 = Unsigned<uint8_t>;
using Int12 = Signed<int16_t>;
using Int16 = Signed<int16_t>;
using Int32 = Signed<int32_t>;

struct Int24
{
    constexpr static auto MAX = pow(2, 23) - 1;
    constexpr static auto MIN = -pow(2, 23);

    Int24(int32_t value = 0);
    int toInt();

private:
    std::array<uint8_t, 3> value_;
};

template <typename T,
          typename = std::enable_if_t<std::is_floating_point<T>::value>>
struct Float
{
    constexpr static T MAX = 1.0;
    constexpr static T MIN = -1.0;

    int toInt()
    {
        constexpr double MAXINT = std::numeric_limits<int>::max();
        constexpr double MININT = std::numeric_limits<int>::min();
        return value_ > 0 ? std::roundl(value_ * MAXINT)
                          : std::roundl(value_ * MININT);
    }

    T value_;
};

using Float32 = Float<float>;
using Float64 = Float<double>;

} // namespace sampletypes

} // namespace mp3encoder

#endif // Q44S7EU5VTZ6QXJLCLJ5242G9TYJU5X2ZDQ1V5MP
