#ifndef I9TJV3TIL2HM27YXOPH144KMKHO396J4M8SG5A67
#define I9TJV3TIL2HM27YXOPH144KMKHO396J4M8SG5A67

#include <memory>
#include <string>
#include <vector>
#include <stdexcept>

#include "iffchunk.h"

namespace mp3encoder {

class WaveFile {
public:
    struct Error : public std::runtime_error { Error(const std::string &); };

    enum class Format
    {
        // Only these formats are supported
        PCM         = 0x0001,
        IEEE_FLOAT  = 0x0003,
    };

    WaveFile(const std::string &fileName = "");
    ~WaveFile();

    Format format();
    size_t channels();
    size_t bits();
    size_t sampleRate();
    size_t samples();

    void rewind();

    template <typename T>
    bool read(std::vector<T> &buf) { return dataChunk().read(buf); }

protected:
    IFFChunk &dataChunk();

private:
    struct Impl;
    std::unique_ptr<Impl> impl;
};

} // namespace mp3encoder

#endif // I9TJV3TIL2HM27YXOPH144KMKHO396J4M8SG5A67
